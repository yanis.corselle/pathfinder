import numpy as np

#initialisation de la carte contenue au sein d'une matrice : les cases "normales" contiennent la valeur 0, le point A la valeur 1 et le point B la valeur 2
carte = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])

# affichage de la carte avant utilisation de l'algorithme du pathfinder
# IN : carte : matrice
# OUT : rien : gère uniquement l'affichage
def AffichageBase(carte):
    for incrementation_lignes in range(len(carte)):
        for incrementation_colonnes in range(len(carte[incrementation_lignes])):
            print(carte[incrementation_lignes][incrementation_colonnes], end=' ')
        print('\n')

# Recherche des chemins possibles
# IN : carte : matrice
# OUT : poids : tableau d'entiers contenant les poids trouvés
def RechercheChemins(carte):
    ligneA, colonneA, ligneB, colonneB, lignes_max, colonnes_max = RecuperationCoordonnees(carte)
    nombre_lignes_min = RecuperationNombresLignes(ligneA, ligneB, lignes_max)
    nombre_colonnes_min = RecuperationNombresColonnes(colonneA, colonneB, colonnes_max)
    poids = RecuerationPoids(nombre_lignes_min, nombre_colonnes_min)
    return poids, nombre_lignes_min, nombre_colonnes_min

# Recherche du nombre de colonnes 
# IN : colonneA, colonneB, colonnes_max : entiers
# OUT : nombre_colonnes : tableau d'entiers
def RecuperationNombresColonnes(colonneA, colonneB, colonnes_max):
    nombre_colonnes_min = list()
    #Récupération de la première possibilité pour le nombre de colonnes maximum
    if colonneA > colonneB :
        nombre_colonnes_min.append(colonneA - colonneB)
    elif colonneB > colonneA :
        nombre_colonnes_min.append(colonneB - colonneA)
    elif colonneB == colonneA :
        nombre_colonnes_min.append(0)


    #Récupération de la deuxième possibilité pour le nombre de colonnes maximum
    nombre_colonnes_min.append(colonnes_max - colonneA)
    nombre_colonnes_min.append(colonnes_max - colonneB)
    return nombre_colonnes_min

# Recherche du nombre de lignes 
# IN : ligneA, ligneB, lignes_max : entiers
# OUT : nombre_lignes : tableau d'entiers
def RecuperationNombresLignes(ligneA, ligneB, lignes_max):
    nombre_lignes_min = list()
    #Récupération de la première possibilité pour le nombre de lignes maximum
    if ligneA > ligneB :
        nombre_lignes_min.append(ligneA - ligneB)
    elif ligneB > ligneA :
        nombre_lignes_min.append(ligneB - ligneA)
    elif ligneB == ligneA :
        nombre_lignes_min.append(0)

    #Récupération de la deuxième possibilité pour le nombre de lignes maximum
    nombre_lignes_min.append(lignes_max - ligneA)
    nombre_lignes_min.append(lignes_max - ligneB)
    return nombre_lignes_min


# Récupération des poids grâce aux lignes et aux colonnes
# IN : nombre_lignes, nombre_colonnes : tableaux d'entiers
# OUT : poids : tableau d'entiers
def RecuerationPoids(nombre_lignes, nombre_colonnes):
    poids = list()
    poids.append(nombre_lignes[0] + nombre_colonnes[0])
    poids.append(nombre_lignes[1] + nombre_colonnes[0])
    poids.append(nombre_lignes[0] + nombre_colonnes[1])

    return poids


# Recherche du poids le plus petit possible parmi ceux trouvés précédemment (qui sont stockés dans un tableau)
# IN : poids : tableau d'entiers contenant les poids trouvés
# OUT : poids_min : entier contenant le poids du chemin le plus rapide
def RecuperationCheminPlusRapide(poids):
    i = 0
    for poid in poids:
        if i == 0:
            poids_min = poid
            i+= 1
        else:
            if(poids_min > poid) :
                poids_min = poid
    return poids_min

# Recherche des coordonnées (ligne et colonne) des points A et B
# IN : carte : matrice
# OUT : ligneA, colonneA, ligneB, colonneB, lignes_max, colonnes_max : entiers
def RecuperationCoordonnees(carte):
    for incrementation_lignes in range(len(carte)):
        for incrementation_colonnes in range(len(carte[incrementation_lignes])):
            if carte[incrementation_lignes][incrementation_colonnes] == 1 :
                ligneA = incrementation_lignes
                colonneA = incrementation_colonnes
            elif carte[incrementation_lignes][incrementation_colonnes] == 2 :
                ligneB = incrementation_lignes
                colonneB = incrementation_colonnes
    lignes_max = incrementation_lignes
    colonnes_max = incrementation_colonnes
    return ligneA, colonneA, ligneB, colonneB, lignes_max, colonnes_max

# affichage du poids minimum des déplacements trouvés
# IN : poids_min : entier
# OUT : rien : gère uniquement l'affichage
def AffichageFinal(poids_min, poids):
    print('Le poids minimum des déplacements est donc de ' + str(poids_min) + ". Les différends poids trouvés sont : ")
    for poid in poids :
        print(poid)

if __name__ == "__main__" :
    AffichageBase(carte)
    poids, nombre_lignes_min, nombre_colonnes_min = RechercheChemins(carte)
    poids_min = RecuperationCheminPlusRapide(poids)
    AffichageFinal(poids_min, poids)