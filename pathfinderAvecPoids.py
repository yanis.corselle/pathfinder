import numpy as np

#initialisation de la carte contenue au sein d'une matrice : les cases "normales" contiennent la valeur de leur poids, le point A la valeur -1, le point B la valeur -2 et les obstacles la valeur 99 (la fonction d'affichage final n'affiche pas les chemins ayant un poids supérieur à 99)
carte1 = np.array([
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -2, 0], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ])

carte2 = np.array([
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, -1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, -2, 0], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ])

carte3 = np.array([
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, -1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 2, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 2, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [2, 2, 3, 2, 2, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 2, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 2, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, -2, 0], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 99, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ])

# affichage de la carte avant utilisation de l'algorithme du pathfinder
# IN : carte : matrice
# OUT : rien : gère uniquement l'affichage
def AffichageBase(carte):
    for incrementation_lignes in range(len(carte)):
        for incrementation_colonnes in range(len(carte[incrementation_lignes])):
            print(carte[incrementation_lignes][incrementation_colonnes], end=' ')
        print('\n')

# Recherche des chemins possibles
# IN : carte : matrice
# OUT : poids : tableau d'entiers contenant les poids trouvés
def RechercheChemins(carte):
    ligneA, colonneA, ligneB, colonneB, lignes_max, colonnes_max = RecuperationCoordonnees(carte)
    nombre_lignes_min = RecuperationNombresLignes(carte, ligneA, ligneB, lignes_max, colonneA, colonneB)
    nombre_colonnes_min = RecuperationNombresColonnes(carte, colonneA, colonneB, colonnes_max, ligneA, ligneB)
    poids = RecuerationPoids(nombre_lignes_min, nombre_colonnes_min)
    return poids, nombre_lignes_min, nombre_colonnes_min

# Recherche du nombre de colonnes 
# IN : colonneA, colonneB, colonnes_max : entiers
# OUT : nombre_colonnes : tableau d'entiers
def RecuperationNombresColonnes(carte, colonneA, colonneB, colonnes_max, ligneA, ligneB):
    nombre_colonnes_min = list()
    #Récupération de la première possibilité pour le nombre de colonnes maximum
    if colonneA > colonneB :
        nombre_colonnes_min.append(RecuperationPoidsDeplacement(carte, colonneA, colonneB, "colonne", ligneA, ligneB))
    elif colonneB > colonneA :
        nombre_colonnes_min.append(RecuperationPoidsDeplacement(carte, colonneB, colonneA, "colonne", ligneA, ligneB))
    elif colonneB == colonneA :
        nombre_colonnes_min.append(0)


    #Récupération de la deuxième possibilité pour le nombre de colonnes maximum
    nombre_colonnes_min.append(RecuperationPoidsDeplacement(carte, colonneB, colonnes_max, "colonne", ligneA, ligneB))
    nombre_colonnes_min.append(RecuperationPoidsDeplacement(carte, colonnes_max, colonneA, "colonne", ligneA, ligneB))
    return nombre_colonnes_min

# Recherche du nombre de lignes 
# IN : ligneA, ligneB, lignes_max : entiers
# OUT : nombre_lignes : tableau d'entiers
def RecuperationNombresLignes(carte, ligneA, ligneB, lignes_max, colonneA, colonneB):
    nombre_lignes_min = list()
    #Récupération de la première possibilité pour le nombre de lignes maximum
    if ligneA > ligneB :
        #nombre_lignes_min.append(ligneA - ligneB)
        nombre_lignes_min.append(RecuperationPoidsDeplacement(carte, ligneA, ligneB, "ligne", colonneA, colonneB))
    elif ligneB > ligneA :
        nombre_lignes_min.append(RecuperationPoidsDeplacement(carte, ligneB, ligneA, "ligne", colonneA, colonneB))
    elif ligneB == ligneA :
        nombre_lignes_min.append(0)

    #Récupération de la deuxième possibilité pour le nombre de lignes maximum
    nombre_lignes_min.append(RecuperationPoidsDeplacement(carte, ligneB, lignes_max, "ligne", colonneA, colonneB))
    nombre_lignes_min.append(RecuperationPoidsDeplacement(carte, lignes_max, ligneA, "ligne", colonneA, colonneB))
    return nombre_lignes_min

#indique le poids d’un déplacement
# IN : carte : matrice d’entiers, emplacement1, emplacement2 : entiers
# OUT : poids : entier
def RecuperationPoidsDeplacement(carte, point1, point2, type_deplacement, point3, point4) :
    poids = 1
    if type_deplacement == "ligne" :
        for i in range(point1, point2+1):
            nouveau_poids = carte[i][point3]
            poids += nouveau_poids
    elif type_deplacement == "colonne" :
        print(point3)
        for i in range(point1, point2+1):
            print("i =" + str(i))
            nouveau_poids = carte[point3][i]
            poids += nouveau_poids
    return poids




# Récupération des poids grâce aux lignes et aux colonnes
# IN : nombre_lignes, nombre_colonnes : tableaux d'entiers
# OUT : poids : tableau d'entiers
def RecuerationPoids(nombre_lignes, nombre_colonnes):
    poids = list()
    poids.append(nombre_lignes[0] + nombre_colonnes[0])
    poids.append(nombre_lignes[1] + nombre_colonnes[0])
    poids.append(nombre_lignes[0] + nombre_colonnes[1])

    return poids


# Recherche du poids le plus petit possible parmi ceux trouvés précédemment (qui sont stockés dans un tableau)
# IN : poids : tableau d'entiers contenant les poids trouvés
# OUT : poids_min : entier contenant le poids du chemin le plus rapide
def RecuperationCheminPlusRapide(poids):
    i = 0
    for poid in poids:
        if i == 0:
            poids_min = poid
            i+= 1
        else:
            if(poids_min > poid) :
                poids_min = poid
    return poids_min

# Recherche des coordonnées (ligne et colonne) des points A et B
# IN : carte : matrice
# OUT : ligneA, colonneA, ligneB, colonneB, lignes_max, colonnes_max : entiers
def RecuperationCoordonnees(carte):
    for incrementation_lignes in range(len(carte)):
        for incrementation_colonnes in range(len(carte[incrementation_lignes])):
            if carte[incrementation_lignes][incrementation_colonnes] == -1 : #case de départ
                ligneA = incrementation_lignes
                colonneA = incrementation_colonnes
            elif carte[incrementation_lignes][incrementation_colonnes] == -2 : #case d'arrivée
                ligneB = incrementation_lignes
                colonneB = incrementation_colonnes
    lignes_max = incrementation_lignes
    colonnes_max = incrementation_colonnes
    return ligneA, colonneA, ligneB, colonneB, lignes_max, colonnes_max

# affichage du poids minimum des déplacements trouvés
# IN : poids_min : entier
# OUT : rien : gère uniquement l'affichage
def AffichageFinal(poids_min, poids):
    print('Le poids minimum des déplacements est donc de ' + str(poids_min) + ". Les différends poids trouvés sont : ")
    for poid in poids :
        if poid < 99 :
            print(poid)

if __name__ == "__main__" :
    AffichageBase(carte2)#modifier la variable pour changer la carte utilisée
    poids, nombre_lignes_min, nombre_colonnes_min = RechercheChemins(carte2)#modifier la variable pour changer la carte utilisée
    poids_min = RecuperationCheminPlusRapide(poids)
    AffichageFinal(poids_min, poids)